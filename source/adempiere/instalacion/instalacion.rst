Instalación de Adempiere 3.8.0 LTS en Debian Jessie
===================================================


Preparando el ambiente
-----------------------

Primeramente necesitamos:

+ Tener instalado y configurado ``debian jessie minimal server`` donde estará nuestro servidor de base de datos y el aplicativo de adempiere.
+ El servidor se instala con los paquetes básicos con los que funciona el sistema operativo (Minimal server)
+ Configurado el archivo /etc/apt/sources.list
+ Configurado el archivo /etc/network/interfaces

Dropbear 
--------

Muchas distribuciones por defecto vienen con OpenSSH como servidor ssh, pero últimamente dropbear es muy popular por la comunidad especialmente por que es liviano.

Instalar dropbear::
	
	$ apt-get install dropbear

Comenzar dropbear::
	
	$ service dropbear start

Si queremos cambiar el puerto por defecto de dropbear(opcional)::

	$ nano /etc/default/dropbear

Configurar dropbear para X11 forwarding editando el archivo ``nano /etc/ssh/ssh_config`` con el objetivo de que el servidor pase a la maquina remota las llamadas al servidor gráfico (opcional)::
	
	#Descomentar linea y cambiar valor a yes
	ForwardX11 yes
	ForwardX11Trusted yes

Reiniciamos el servicor ssh::

	$ service dropbear restart

Ahora ya podemos conectarnos remotamente a nuestra maquina: ssh -X *direcccion-ip*

Java
-----

JRE (Java Runtime Environment) & JDK (Java Development Kit) 
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
::

	$ apt-get install default-jre
	$ apt-get install default-jdk

Verificar la instalación::

	$ java -version

Establecer las Variables Globales
---------------------------------

Editamos el archivo ``/etc/enviroment``::

	JAVA_HOME=/usr/lib/jvm/java-7-openjdk-amd64
	ADEMPIERE_HOME=/opt/Adempiere

Creamos un archivo en ``/etc/profile.d/`` llamado ``myvariable.sh`` y le agregamos::
	
	JAVA_HOME=/usr/lib/jvm/java-7-openjdk-amd64
	ADEMPIERE_HOME=/opt/Adempiere	

cargamos las variables::

	$ source /etc/enviroment
	$ source /etc/profile.d/myvariable.sh

Verificamos si se cargaron::

	$ echo $JAVA_HOME
	$ echo $ADEMPIERE_HOME

.. note::
	Si las variables se cargaron correctamente, el comando ``echo`` debería de imprimir el valor establecido de dichas variables::
	
		/usr/lib/jvm/java-7-openjdk-amd64
		/opt/Adempiere


Postgresql
----------

Agregar el repositorio de Postgresql a nuestro sistema, con el fin de obtener las ultimas actualizaciones::

	sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt/ $(lsb_release -cs)-pgdg main" > /etc/apt/sources.list.d/pgdg.list'

Importar la llave del repositorio de https://www.postgresql.org/media/keys/ACCC4CF8.asc y actualizar los paquetes::

	$ apt-get install wget ca-certificates
	$ wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | apt-key add -
	$ apt-get update
	$ apt-get upgrade

Instalación de Paquetes::

	$ apt-get install postgresql postgresql-contrib

Nos logeamos con postgres y entramos a psql::

	$ su postgres
	$ psql postgres

En psql, agregar contraseña al usuario administrador postgres::
	
	$ alter user postgres with password 'tu_contrasenia';

Salimos de psql y del usuario postgres::

	$ \q  #salimos de psql
	$ exit #salimos del usuario postgres

Abrimos el archivo ``/etc/postgresql/<version>/main/postgresql.conf`` y descomentamos la linea siguiente::

	listen_addresses = '*'	# le modificamos a * para que escuche a cualquier cliente en la red

Configurar lista de acceso en ``/etc/postgresql/<version>/main/pg_hba.conf`` ::
	
	# IPv4 local connections:
	#host all all 127.0.0.1/32  md5
	host  all all 0.0.0.0/0     md5

Reiniciar servicios::

	$ service postgresql restart
	$ service postgresql reload

Proseguimos con la creación del usuario adempiere::
	
	$ su postgres
	$ create user adempiere -d -l -r -s -P --replication

.. note::
    + d : se le permite crear base de datos
    + l : se le permite logeo
    + r : se le permite crear nuevos roles
    + s : es superusuario
    + P : se le solicita automaticamente una contraseña
    + replication : se le permite hacer replicación

Creamos la Base de Datos::

	$ createdb -h host_db -p 5432 -U adempiere -d nombre_db

Adempiere
---------

Descargar adempiere versión 3.8.0: https://bintray.com/adempiere/Official-Repository/Release

Descomprimir::
	
	$ tar -xvzf nombre_archivo.tar.gz

Mover a opt::
	
	$ mv Adempiere /opt/
	$ cd /opt/Adempiere

Damos permisos de ejecución a todos los archivos .sh::

	$ find utils . -maxdepth 1 -iname '*.sh' -exec chmod +x {} \;
	$ chmod 775 *.sh 

Para hacer la configuración sin la interfaz gráfica primero copiamos el archivo ``AdempiereEnvTemplate.properties``::

	$ cp AdempiereEnvTemplate.properties AdempiereEnv.properties

El archivo ``AdempiereEnv.properties`` debería verse así::

	ADEMPIERE_DB_PATH=postgresql
	ADEMPIERE_DB_SERVER=ip_del_server 
	ADEMPIERE_DB_TYPE=postgresql
	ADEMPIERE_DB_PORT=5432
	ADEMPIERE_DB_PASSWORD=adempiere #contrasenia del usuario de la base de datos
	ADEMPIERE_DB_NAME=nombre_bd #nombre de la base de datos
	ADEMPIERE_DB_SYSTEM=123 #contrasenia del DB System, en nuestro caso que es postgres
	ADEMPIERE_HOME=/opt/Adempiere
	JAVA_HOME=/usr/lib/jvm/java-7-openjdk-amd64
	ADEMPIERE_JAVA_TYPE=OpenJDK
	ADEMPIERE_JNP_PORT=1099
	ADEMPIERE_APPS_SERVER=ip_del_server
	ADEMPIERE_SSL_PORT=8443
	ADEMPIERE_WEB_PORT=8082
	ADEMPIERE_KEYSTORECODEALIAS=adempiere
	ADEMPIERE_KEYSTOREPASS=adempiere
	ADEMPIERE_KEYSTOREWEBALIAS=adempiere
	ADEMPIERE_KEYSTORE=/opt/Adempiere/keystore/myKeystore
	ADEMPIERE_CERT_COUNTRY=US
	ADEMPIERE_CERT_ORG_UNIT=AdempiereUser
	ADEMPIERE_CERT_LOCATION=myTown
	ADEMPIERE_CERT_CN=localhost
	ADEMPIERE_CERT_ORG=ADempiere Bazaar
	ADEMPIERE_CERT_STATE=CA
	ADEMPIERE_MAIL_SERVER=ip_del_server
	ADEMPIERE_ADMIN_EMAIL=
	ADEMPIERE_MAIL_USER=
	ADEMPIERE_MAIL_UPDATED=
	ADEMPIERE_MAIL_PASSWORD=
	ADEMPIERE_FTP_SERVER=localhost
	ADEMPIERE_FTP_PREFIX=my
	ADEMPIERE_FTP_PASSWORD=user@host.com
	ADEMPIERE_FTP_USER=anonymous


Agregar la llave::

	keytool -genkey -keyalg rsa -alias adempiere -dname "CN=localhost, OU=ADempiere Bazaar, O=ADempiere User, L=Sydney, S=NSW, C=AU" -keypass adempiere -validity 999 -keystore /opt/Adempiere/keystore/myKeystore -storepass adempiere

Dar permisos al archivo ``myKeystore``::

	chmod 775 /opt/Adempiere/keystore/myKeystore

Corremos el ``./RUN_silentsetup.sh``

.. note::
	Al final de la ejecución ``./RUN_silentsetup.sh`` puede dar un error relacionado con la base de datos, o relacionado con ad_table, este error se soluciona con la importación de la base de datos: ``./RUN_ImportAdempiere.sh`` que es el siguiente paso a realizar. Si no hay ningún error mas, y se muestra el mensaje **BUILD SUCCESSFUL** se puede proseguir.

Importamos la Base de Datos::
	
	./RUN_ImportAdempiere.sh

Iniciamos el servidor::
	
	./RUN_Server2.sh

.. tip::
	También podemos iniciar el servidor con el comando **nohup** para ejecutar adempiere en *background*, permitiéndonos cerrar la consola: ``nohup ./RUN_Server2.sh &``. Si queremos detener el servicio de adempiere ejecutamos ``./RUN_Server2Stop.sh`` o bien matamos el proceso de java.

Para probar si todo esta bien vía web, en nuestro navegador ingresamos::
	
	host:PuertoWeb/admin

Para ingresar al sistema por defecto podemos iniciar con el usuario **GardenAdmin** y contraseña **GardenAdmin**, o bien con el usuario **System** con contraseña **System**

Desinstalar completamente Adempiere, Postgres, JAVA
---------------------------------------------------

Desinstalar Adempiere
~~~~~~~~~~~~~~~~~~~~~

::

	cd $ADEMPIERE_HOME/utils/
	./RUN_Server2Stop.sh
	cd /opt/
	rm -rf Adempiere

Desinstalar JAVA
~~~~~~~~~~~~~~~~

Desinstalar todos los paquetes relacionados con java::

	dpkg-query -W -f='${binary:Package}\n' | grep -E -e '^(ia32-)?(sun|oracle)-java' -e '^openjdk-' -e '^icedtea' -e '^(default|gcj)-j(re|dk)' -e '^gcj-(.*)-j(re|dk)' -e '^java-common' | xargs sudo apt-get -y remove apt-get -y autoremove

Remover archivos de configuración::

	dpkg -l | grep ^rc | awk '{print($2)}' | xargs sudo apt-get -y purge

Remover manualmente *JVMs*::

	rm -rf /usr/lib/jvm/*

Remover entradas de java::

	for g in ControlPanel java java_vm javaws jcontrol jexec keytool mozilla-javaplugin.so orbd pack200 policytool rmid rmiregistry servertool nameserv unpack200 appletviewer apt extcheck HtmlConverter idlj jar jarsigner javac javadoc javah javap jconsole jdb jhat jinfo jmap jps jrunscript jsadebugd jstack jstat jstatd native2ascii rmic schemagen serialver wsgen simport xjc xulrunner-1.9-javaplugin.so; do sudo update-alternatives --remove-all $g; done

Desinstalar postgresql
~~~~~~~~~~~~~~~~~~~~~~

Ver que paquetes desinstalar::

	dpkg -l | grep postgresql

Eliminar archivos de configuración y paquete::

	apt-get --purge remove postgresql\

Eliminar directorios relacionados::

	rm -r /etc/postgresql/
	rm -r /etc/postgresql-common/
	rm -r /var/lib/postgresql/
	userdel -r postgres
	groupdel postgres


Links & Referencias
-------------------
+ Instalación debian minimal server
    * https://www.howtoforge.com/tutorial/debian-8-jessie-minimal-server/
+ Java
	* https://wiki.debian.org/Java
+ Postgresql
    * https://wiki.postgresql.org/wiki/Apt
+ SSH forwarding
	* http://superuser.com/questions/795928/howto-start-an-application-remotely-so-that-gui-is-shown-locally
	* http://blog.desdelinux.net/x11-forwarding-a-traves-de-ssh/
+ Keytool Adempiere
	* https://sourceforge.net/p/adempiere/discussion/610546/thread/8313b78e/
+ Desinstalación
	* http://stackoverflow.com/questions/2748607/how-to-thoroughly-purge-and-reinstall-postgresql-on-ubuntu
	* http://askubuntu.com/questions/84483/how-to-completely-uninstall-java
