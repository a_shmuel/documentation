Pgbouncer
==========

Instalando pgbouncer en debian jessie
-------------------------------------

Instalar desde los repositorios:: 
	
	$ apt-get install pgbouncer

.. note::
    Esta opción suele dar algunos problemas ya que la versión que se encuentra en los repositorios no es la mas actual, lo preferente es instalar mediante la compilación.

Instalación mediante compilación
--------------------------------

Descargamos del sitio web oficial pgbouncer_ en la sección de downloads, nos percatamos de optar por una versión estable, en nuestro caso descargamos la ``version 1.7``

Descomprimir el archivo descargado::
	
	$ tar -xvzf pgbouncer-1.7.tar.gz

Instalar dependencias previas:

+ ``apt-get install make`` *(metapaquete para compilación)*
+ ``apt-get install built-essential`` *(metapaquete para compilación)*
+ ``apt-get install libevent-dev``
+ ``apt-get install openssl``
+ ``apt-get install libssl-dev`` *(headers para openssl)*
+ ``apt-get install c-ares`` *(opcional)*

Compilar::
	
	$ ./configure --prefix=/usr/local --with-libevent=libevent-prefix
	$ make
	$ make install

Configuración
--------------

Pgbouncer crea estos dos directorios:

+ pgbouncer(archivo binario): ``/usr/local/bin``
+ archivos de configuración: ``/usr/local/share/doc/pgbouncer``

Configurar ``pgbouncer.ini``::
	
	$ cd /usr/local/share/doc/pgbouncer/
	$ nano pgbouncer.ini

El archivo ``pgbouncer.ini`` se divide en secciones las cuales son señaladas con: **[section_name]**, y los comentarios comienzan con una o dos: **;** o con: **#**.

En el archivo ``pgbouncer.ini`` configurar las siguientes opciones::

	
    [database]
    nombre_baseDatos = host=localhost port=5432 dbname=nombre_baseDatos

    [pgbouncer]
    listen_port = 6543
    listen_addr = localhost
    admin_users = algun_usuario, otro_usuario

    ; ip address or * which means all ip-s
    listen_addr = *
    ;listen_port = 5433 #(comentar esta linea)

    ; any, trust, plain, crypt, md5
    auth_type = md5
    auth_file = /usr/local/share/doc/pgbouncer/userlist.txt

    lofile = pgbouncer.log #(este archivo hay que crearlo en /usr/local/share/doc/pgbouncer)

    pidfile = pgbouncer.pid

    ; comma-separed list of users who are allowed to change settins
    admin_users = postgres, otherusers

    ; comma-separed list of users who are allowed to use SHOW command
    stats_users = postgres, otherusers

    pool_mode = transaction

    max-client_conn = numero_maximo_conexion
	
Ahora agregar usuarios a ``userlist.txt`` (usuarios permitidos)::
	
	"username" "password"
	"username" "password"

.. Tip:: Ambos archivos deben de estar adecuadamente con permisos y propietario, lo preferible es que sea el propietario el usuario **postgres**.

Al final en el directorio local debe de quedar con los archivos: ``pgbouncer.ini``, ``pgbouncer.log``, ``userlist.txt``

Uso
---

Para ver info de como usar el comando pgbouncer::
	
	$ pgbouncer -h

Corremos el pgbouncer con el usuario postgres (Si corremos pgbouncer como root nos daría error
)::
	
	$ pgbouncer -d pgbouncer.ini -v

probamos si esta levantado el demonio::

	$ ps aux | grep pgbouncer

ahora entramos en la consola de administración::
	
	$ psql -h localhost -p 6543 -U postgres -d pgbouncer

para uso de la consola de administración::
	
	$ show help; #muestra los comandos de administración

por ejemplo para ver el pool de conexiones::

	$ show pools;

Si modificamos el archivo ``pgboucer.ini`` no es necesario reiniciar el servicio pgbouncer, solo entramos a la consola de administración::
	
	$ psql -h localhost -p 6543 -U postgres -d pgbouncer
	$ RELOAD; 

Prueba (Benchmark)
~~~~~~~~~~~~~~~~~~

Para hacer la simulación de múltiples conexiones por cliente necesitamos **pgbench** que por defecto viene en el paquete **contrib de postgres**.

Inicializar pgbench::
	
	su postgres	
	pgbench -i -s 10 database_name

+ -i = inicializar pgbench, crea 4 tablas:
    * pgbench_accounts
    * pgbench_branches
    * pgbench_history
    * pgbench_tellers
+ -s = factor de escala, por ejemplo -s 100 creara 10000000 filas en la tabla pgbench_accounts

Comparando el Performance
~~~~~~~~~~~~~~~~~~~~~~~~~

Comparamonos entre nuestros distintos puertos de las bases de datos, el 5432 que es por defecto y el 6543 del pgbouncer. Pgbench arroja los **TPS** (transacciones por segundo).

Performance con 10 clientes en el puerto 5432::
	
	pgbench -h localhost -c 10 -C -T 60 database_name

Performance con 10 clientes en el puerto 6543::
	
	pgbench -h localhost -p 6543 -c 10 -C -T 60 database_name

+ -c = número de clientes
+ -C = establece una nueva conexión por cada transacción
+ -T = tiempo de la prueba en segundos

Links & Reference
-----------------
+ Introduccion to connection pool
    * https://wiki.postgresql.org/wiki/Number_Of_Database_Connections
    * https://blogs.walkingtree.in/2013/03/07/seperate-database-for-read-and-write-in-adempiere/
+ Adempiere connection pool
    * https://sourceforge.net/p/adempiere/discussion/611163/thread/75d38a89/
    * https://sourceforge.net/p/adempiere/discussion/610548/thread/6caffc1f/
    * https://sourceforge.net/p/adempiere/discussion/610548/thread/94a19a69/
+ Pgbouncer
    * https://pgbouncer.github.io/usage.html
    * https://opensourcedbms.com/dbms/setup-pgbouncer-connection-pooling-for-postgresql-on-centosredhatfedora/
+ Pgbench
    * http://www.cybertec.at/2013/05/pgbouncer-proving-the-point/
    * https://www.postgresql.org/docs/devel/static/pgbench.html
    * http://gpdb.docs.pivotal.io/4360/admin_guide/access_db/topics/pgbouncer.html

.. _pgbouncer: https://pgbouncer.github.io/

.. disqus::