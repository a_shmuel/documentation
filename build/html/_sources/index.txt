Documentación!
======================================

La documentación para este sitio esta organizada en las siguientes secciones:

.. toctree::
   :maxdepth: 3
   :caption: Postgres

   postgres/instalacion/instalacion
   postgres/pgbouncer/pgbouncer

.. toctree::
   :maxdepth: 3
   :caption: Adempiere

   adempiere/instalacion/instalacion
